module bitbucket.org/mfkenney/go-redisq/v2

go 1.16

require (
	github.com/alicebob/miniredis/v2 v2.30.0
	github.com/gomodule/redigo v1.8.5
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	google.golang.org/appengine v1.6.7 // indirect
)
