// Package implements a FIFO queue using a Redis list. Queue elements
// are MessagePack encoded.
package redisq

import (
	"context"
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/gomodule/redigo/redis"
	msgpack "github.com/vmihailenco/msgpack/v5"
)

type Queue[T any] struct {
	conn   redis.Conn
	key    string
	maxlen uint
	elem   T
}

// Queue elements
type entry struct {
	// Data source name
	Src string `msgpack:"src"`
	// Timestamp in nanoseconds since 1/1/1970 UTC
	T int64 `msgpack:"t"`
	// Arbitrary data
	Data any `msgpack:"data"`
}

// Return a new Redis-based Queue.
func NewQueue[T any](conn redis.Conn, key string, maxlen uint) *Queue[T] {
	return &Queue[T]{conn: conn, key: key, maxlen: maxlen}
}

// Create a new entry
func newEntry(src string, t time.Time, data any) entry {
	return entry{Src: src, T: t.UnixNano(), Data: data}
}

// IsZero reports whether the entry is a zero value (i.e. empty)
func (e entry) IsZero() bool {
	return e.Src == "" && e.T == 0 && e.Data == nil
}

// Timestamp returns the time.Time of the entry
func (e entry) Timestamp() time.Time {
	return time.Unix(0, e.T)
}

// String implements the fmt.Stringer interface
func (e entry) String() string {
	var b strings.Builder
	fmt.Fprintf(&b, "%s %s", e.Timestamp().Format(time.RFC3339), e.Src)
	switch data := e.Data.(type) {
	case map[string]interface{}:
		keys := make([]string, 0, len(data))
		for k := range data {
			keys = append(keys, k)
		}
		sort.Strings(keys)
		for _, k := range keys {
			fmt.Fprintf(&b, " %s=%v", k, data[k])
		}
	case string, []byte:
		fmt.Fprintf(&b, " data=%q", data)
	default:
		fmt.Fprintf(&b, " data=%v", data)
	}
	return b.String()
}

// Put appends a new entry to q, the entry is MessagePack encoded.
func (q *Queue[T]) Put(e T) error {
	b, err := msgpack.Marshal(e)
	if err != nil {
		return fmt.Errorf("msgpack encode %#v: %w", e, err)
	}

	_, err = q.conn.Do("LPUSH", q.key, b)
	if err != nil {
		return fmt.Errorf("Redis LPUSH: %w", err)
	}

	if q.maxlen > 0 {
		_, err = q.conn.Do("LTRIM", q.key, 0, q.maxlen-1)
	}

	return err
}

// GetBlock returns the oldest entry from a Queue. If the Queue is empty, it
// blocks until a new entry is added or until the Context expires.
func (q *Queue[T]) GetBlock(ctx context.Context) (T, error) {
	reply, err := redis.DoContext(q.conn, ctx, "BRPOP", q.key, 0)
	if err != nil {
		return q.elem, err
	}

	vals, err := redis.Values(reply, err)
	if err != nil {
		return q.elem, err
	}
	err = msgpack.Unmarshal(vals[1].([]byte), &q.elem)
	return q.elem, err
}

func (q *Queue[T]) Get() (T, error) {
	reply, err := redis.Bytes(q.conn.Do("RPOP", q.key))
	if err != nil {
		return q.elem, err
	}

	err = msgpack.Unmarshal(reply, &q.elem)
	return q.elem, err
}

// Return the Queue length
func (q *Queue[T]) Len() int {
	n, _ := redis.Int(q.conn.Do("LLEN", q.key))
	return n
}

// Remove all entries from the Queue
func (q *Queue[T]) Clear() error {
	_, err := q.conn.Do("DEL", q.key)
	return err
}
