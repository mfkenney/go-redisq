package redisq

import (
	"context"
	"errors"
	"fmt"
	"net"
	"testing"
	"time"

	miniredis "github.com/alicebob/miniredis/v2"
	"github.com/gomodule/redigo/redis"
	"github.com/google/go-cmp/cmp"
)

func helperDialRedis(t *testing.T) redis.Conn {
	s := miniredis.RunT(t)
	conn, err := redis.Dial("tcp", s.Addr())
	if err != nil {
		t.Fatal(err)
	}
	return conn
}

func TestBasic(t *testing.T) {
	q := NewQueue[entry](helperDialRedis(t), "testq", 0)
	defer q.Clear()

	ts := time.Now()
	want := newEntry("test", ts, map[string]any{"a": int8(1), "b": 2.5, "c": "hello"})
	err := q.Put(want)
	if err != nil {
		t.Errorf("q.Put: %v", err)
	}

	if q.Len() != 1 {
		t.Errorf("Bad queue length: %d", q.Len())
	}

	got, err := q.Get()
	if err != nil {
		t.Errorf("q.Get: %v", err)
	}

	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("Queue.Get() mismatch (-want +got):\n%s", diff)
	}
}

func TestEmpty(t *testing.T) {
	q := NewQueue[entry](helperDialRedis(t), "testq", 0)
	defer q.Clear()

	_, err := q.Get()
	if !errors.Is(err, redis.ErrNil) {
		t.Errorf("Unexpected error type: %v", err)
	}
}

func TestLimit(t *testing.T) {
	q := NewQueue[entry](helperDialRedis(t), "testq", 2)
	defer q.Clear()

	for i := 0; i < 4; i++ {
		err := q.Put(newEntry("item", time.Now(), "hello"))
		if err != nil {
			t.Errorf("q.Put: %v", err)
		}
	}

	if q.Len() != 2 {
		t.Errorf("Bad queue length: %d", q.Len())
	}
}

func isNetTimeout(err error) bool {
	var opError *net.OpError

	return errors.As(err, &opError) && opError.Timeout()
}

func TestBlocking(t *testing.T) {
	q := NewQueue[entry](helperDialRedis(t), "testq", 2)
	defer q.Clear()

	ts := time.Now()
	want := newEntry("test", ts, map[string]any{"a": int8(1), "b": 2.5, "c": "hello"})
	err := q.Put(want)
	if err != nil {
		t.Errorf("q.Put: %v", err)
	}

	ctx0 := context.Background()

	got, err := q.GetBlock(ctx0)
	if err != nil {
		t.Errorf("q.Get: %v", err)
	}

	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("Queue.GetBlock() mismatch (-want +got):\n%s", diff)
	}

	if q.Len() != 0 {
		t.Error("Bad Queue length")
	}

	ctx, cancel := context.WithTimeout(ctx0, time.Second*2)
	defer cancel()

	// Queue is empty so this call should timeout
	_, err = q.GetBlock(ctx)
	if !errors.Is(err, context.DeadlineExceeded) &&
		!isNetTimeout(err) {
		t.Errorf("Unexpected error type: %v", err)
	}
}

func TestEntryString(t *testing.T) {
	ts := time.Now()
	fmtTs := ts.Format(time.RFC3339)
	e := newEntry("test", ts, map[string]any{"b": 2.5, "c": "hello", "a": int8(1)})
	want := fmt.Sprintf("%s test a=%d b=%.1f c=hello", fmtTs, 1, 2.5)
	got := e.String()

	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("Entry.String() mismatch (-want +got):\n%s", diff)
	}

	e2 := newEntry("test", ts, []byte("hello world"))
	want = fmt.Sprintf("%s test data=\"hello world\"", fmtTs)
	got = e2.String()
	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("Entry.String() mismatch (-want +got):\n%s", diff)
	}
}
