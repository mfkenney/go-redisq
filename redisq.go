// Package implements a data queue using a Redis list. Queue elements
// are MessagePack encoded.
package redisq

import (
	"errors"
	"fmt"

	"github.com/gomodule/redigo/redis"
	"github.com/vmihailenco/msgpack"
)

type Queue struct {
	conn   redis.Conn
	key    string
	maxlen uint
}

var ErrTimeout = errors.New("Timeout reading queue")

// Return a new Redis-based Queue. The Queue address may be specified as either
// a network address string (ala net.Dial) or a redis.Conn instance.
func NewQueue(address interface{}, key string, maxlen uint) (*Queue, error) {
	var err error
	var conn redis.Conn

	switch addr := address.(type) {
	case string:
		conn, err = redis.Dial("tcp", addr)
	case redis.Conn:
		conn = addr
		err = nil
	default:
		err = errors.New("Invalid address type")
	}

	if err != nil {
		return nil, err
	}
	q := Queue{conn: conn, key: key, maxlen: maxlen}
	return &q, nil
}

// Put a new value in the Queue
func (q *Queue) Put(vals ...interface{}) error {
	var (
		buf []byte
		err error
	)

	buf, err = msgpack.Marshal(vals)
	if err != nil {
		return fmt.Errorf("encoding failed: %w", err)
	}

	_, err = q.conn.Do("LPUSH", q.key, buf)
	if err != nil {
		return fmt.Errorf("LPUSH failed: %w", err)
	}

	if q.maxlen > 0 {
		_, err = q.conn.Do("LTRIM", q.key, 0, q.maxlen-1)
	}

	return err
}

// Get the oldest value from the Queue
func (q *Queue) Get(block bool, timeout uint) ([]interface{}, error) {
	var (
		buf []byte
	)

	rec := make([]interface{}, 0)
	if block {
		reply, err := redis.Values(q.conn.Do("BRPOP", q.key, timeout))
		if err != nil {
			if err == redis.ErrNil {
				return nil, fmt.Errorf("%s: %w", q.key, ErrTimeout)
			}
			return nil, err
		}
		buf = reply[1].([]byte)
	} else {
		reply, err := redis.Bytes(q.conn.Do("RPOP", q.key))
		if err != nil {
			return nil, err
		}
		buf = reply
	}

	err := msgpack.Unmarshal(buf, &rec)

	return rec, err
}

// Return the Queue length
func (q *Queue) Len() int {
	n, _ := redis.Int(q.conn.Do("LLEN", q.key))
	return n
}

// Remove all entries from the Queue
func (q *Queue) Clear() error {
	_, err := q.conn.Do("DEL", q.key)
	return err
}
