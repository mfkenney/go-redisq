module bitbucket.org/mfkenney/go-redisq

go 1.16

require (
	github.com/gomodule/redigo v1.8.5
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	google.golang.org/appengine v1.6.7 // indirect
)
