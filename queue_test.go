package redisq

import (
	"errors"
	"testing"

	"github.com/gomodule/redigo/redis"
)

func TestBasic(t *testing.T) {
	q, err := NewQueue("localhost:6379", "testq", 0)
	if err != nil {
		t.Skip("No Redis server available")
	}
	defer q.Clear()

	err = q.Put(int(42), int(1013), "testing", float64(3.14159))
	if err != nil {
		t.Errorf("q.Put: %v", err)
	}

	if q.Len() != 1 {
		t.Errorf("Bad queue length: %d", q.Len())
	}

	vals, err := q.Get(false, 0)
	if err != nil {
		t.Errorf("q.Get: %v", err)
	}

	if len(vals) != 4 {
		t.Errorf("Decode error: %v", vals)
	}

	f, ok := vals[3].(float64)
	if !ok || f != 3.14159 {
		t.Errorf("Decode error: %v", vals)
	}
}

func TestConn(t *testing.T) {
	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		t.Skip("No Redis server available")
	}
	_, err = NewQueue(conn, "testq", 0)
	if err != nil {
		t.Error(err)
	}
}

func TestLimit(t *testing.T) {
	q, err := NewQueue("localhost:6379", "testq", 2)
	if err != nil {
		t.Skip("No Redis server available")
	}
	defer q.Clear()

	for i := 0; i < 4; i++ {
		err = q.Put(int(42), int(1013), "testing", float64(3.14159))
		if err != nil {
			t.Errorf("q.Put: %v", err)
		}
	}

	if q.Len() != 2 {
		t.Errorf("Bad queue length: %d", q.Len())
	}
}

func TestBlocking(t *testing.T) {
	q, err := NewQueue("localhost:6379", "testq", 2)
	if err != nil {
		t.Skip("No Redis server available")
	}
	defer q.Clear()

	err = q.Put(int(42), int(1013), "testing", float64(3.14159))
	if err != nil {
		t.Errorf("q.Put: %v", err)
	}

	_, err = q.Get(true, 0)
	if err != nil {
		t.Errorf("q.Get: %v", err)
	}

	if q.Len() != 0 {
		t.Error("Bad Queue length")
	}

	_, err = q.Get(true, 1)
	if !errors.Is(err, ErrTimeout) {
		t.Errorf("Unexpected error type: %v", err)
	}
}

func TestEmpty(t *testing.T) {
	q, err := NewQueue("localhost:6379", "testq", 2)
	if err != nil {
		t.Skip("No Redis server available")
	}
	defer q.Clear()

	err = q.Put()
	if err != nil {
		t.Errorf("q.Put <empty>: %v", err)
	}

	vals, err := q.Get(false, 0)
	if err != nil {
		t.Errorf("q.Get: %v", err)
	}
	if len(vals) != 0 {
		t.Errorf("Decode error: %v", vals)
	}
}
