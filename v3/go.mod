module bitbucket.org/mfkenney/go-redisq/v3

go 1.21

require (
	github.com/alicebob/miniredis/v2 v2.30.0
	github.com/gomodule/redigo v1.8.9
	github.com/google/go-cmp v0.6.0
	github.com/vmihailenco/msgpack/v5 v5.4.1
)

require (
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	github.com/yuin/gopher-lua v0.0.0-20220504180219-658193537a64 // indirect
)
